package com.avinty.rest;

import com.avinty.entity.Department;
import com.avinty.entity.Employee;
import com.avinty.entity.Entity;
import com.avinty.service.DepartmentService;
import com.avinty.service.EmployeeService;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.stream.Stream;

@Log4j2
@Path("/dep-emp")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DepEmpRestController {

    private final EmployeeService employeeService;
    private final DepartmentService departmentService;

    public DepEmpRestController() {
        this.employeeService = new EmployeeService();
        this.departmentService = new DepartmentService();
    }

    @GET
    public Response listAll(){
        List<Employee> employees = this.employeeService.listAll();
        List<Department> departments = this.departmentService.listAll();

        List<Entity> container = Stream.concat(employees.stream(), departments.stream()).toList();

        return Response.ok(container).build();
    }

}

