package com.avinty.rest;

import com.avinty.entity.Department;
import com.avinty.service.DepartmentService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

import java.util.List;

@Log4j2
@Path("/department")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DepartmentRestController{
    private final DepartmentService departmentService;

    public DepartmentRestController() {
        this.departmentService = new DepartmentService();
    }


    @GET
    @Path("/listAll")
    public Response listAll() {
        List<Department> departments = this.departmentService.listAll();

        return Response.ok(departments).build();
    }

    @GET
    @Path("/{name}")
    public Response findByName(@PathParam("name") String name) {
        if(name == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Department departmentByName = this.departmentService.findByName(name);

        return Response.ok(departmentByName).build();
    }

    @POST
    @Path("/save")
    public Response save(Department department) {
        if(department == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Department savedDepartment = this.departmentService.save(department);

        return Response.ok(savedDepartment).build();
    }

    @DELETE
    @Path("/delete/{id}")
    public Response delete(@PathParam("id") Long id) {
        if(id == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        this.departmentService.delete(id);

        return Response.ok().build();
    }
}

