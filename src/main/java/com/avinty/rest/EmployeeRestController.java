package com.avinty.rest;

import com.avinty.entity.Employee;
import com.avinty.service.EmployeeService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

import java.util.List;

@Log4j2
@Path("/employees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeRestController{

    private final EmployeeService employeeService;

    public EmployeeRestController() {
        this.employeeService = new EmployeeService();
    }


    @GET
    /*@Path("/listAll")*/
    public Response listAll() {
        List<Employee> employees = this.employeeService.listAll();

        return Response.ok(employees).build();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id) {
        if(id == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Employee employeeById = this.employeeService.findById(id);

        return Response.ok(employeeById).build();
    }

    @POST
    /*@Path("/save")*/
    public Response save(Employee employee) {
        if(employee == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Employee savedEmployee = this.employeeService.save(employee);

        return Response.ok(savedEmployee).build();
    }
}

