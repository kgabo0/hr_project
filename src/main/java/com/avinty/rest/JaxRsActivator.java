package com.avinty.rest;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/API/V1")
public class JaxRsActivator extends Application {
}

