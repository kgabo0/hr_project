package com.avinty.rest;

import com.avinty.entity.Entity;
import jakarta.ws.rs.core.Response;

public interface EntityRestController<T extends Entity>{

    Response listAll();

    Response findById(Long id);

    /*Response findByName(String name);*/

    Response save(T entity);

    Response delete(Long id);

}

