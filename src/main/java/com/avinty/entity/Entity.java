package com.avinty.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class Entity {

    private Date createdAt;
    private Long createdBy;
    private Date updatedAt;
    private Long updatedBy;



}

