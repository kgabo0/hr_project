package com.avinty.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Employee extends Entity {

    private Long id;
    private String email;
    private String password;
    private String fullName;
    private Long depId;

    public Employee(Date createdAt, Long createdBy, Date updatedAt, Long updatedBy, Long id, String email, String password, String fullName, Long depId) {
        super(createdAt, createdBy, updatedAt, updatedBy);
        this.id = id;
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.depId = depId;
    }

    public Employee() {

    }
}

