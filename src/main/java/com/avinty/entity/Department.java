package com.avinty.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Department extends Entity {

    private Long id;
    private String name;
    private Long managerId;

    public Department(Date createdAt, Long createdBy, Date updatedAt, Long updatedBy, Long id, String name, Long managerId) {
        super(createdAt, createdBy, updatedAt, updatedBy);
        this.id = id;
        this.name = name;
        this.managerId = managerId;
    }

    public Department() {

    }
}

