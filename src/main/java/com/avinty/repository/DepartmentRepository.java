package com.avinty.repository;

import com.avinty.entity.Department;
import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DepartmentRepository extends Repository implements EntityRepository<Department>{

    private static DepartmentRepository instance;

    public static DepartmentRepository getInstance() {
        if (instance == null) {
            instance = new DepartmentRepository();
        }
        return instance;
    }

    @Override
    public Department save(Department department) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "INSERT INTO departments (name, manager_id, created_at, created_by, updated_at, updated_by) VALUES (?, ?, ?, ?, ?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {

            stmt.setString(1, department.getName());
            stmt.setLong(2, department.getManagerId());
            stmt.setDate(5, (Date) department.getCreatedAt());
            stmt.setLong(6, department.getCreatedBy());
            stmt.setDate(7, (Date) department.getUpdatedAt());
            stmt.setLong(8, department.getUpdatedBy());

            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            generatedKeys.next();

            return this.findById(generatedKeys.getLong(1));
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Department findById(Long id) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "SELECT id, name, manager_id, created_at, created_by, updated_at, updated_by FROM departments WHERE id = ?")) {

            stmt.setLong(1, id);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            return this.mapDepartment(rs);
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Department findByName(String name) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "SELECT id, name, manager_id, created_at, created_by, updated_at, updated_by FROM departments WHERE name = ?")) {

            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            return this.mapDepartment(rs);
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Department> listAll() {
        try (Connection con = this.getConnection(); Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery("SELECT id, name, manager_id, created_at, created_by, updated_at, updated_by FROM  departments");
            List<Department> departments = new ArrayList<>();

            while (rs.next()) {
                departments.add(this.mapDepartment(rs));
            }

            return departments;
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }


    @Override
    public boolean delete(Long id) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "DELETE FROM departments WHERE id = ?")) {
            stmt.setLong(1, id);
            return stmt.executeUpdate() != 0;
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private Department mapDepartment(ResultSet rs) throws SQLException {
        Department department = new Department();
        department.setId(rs.getLong("id"));
        department.setName(rs.getString("name"));
        department.setManagerId(rs.getLong("manager_id"));
        department.setCreatedAt(rs.getDate("created_at"));
        department.setCreatedBy(rs.getLong("created_by"));
        department.setUpdatedAt(rs.getDate("updated_at"));
        department.setUpdatedBy(rs.getLong("updated_by"));

        return department;
    }
}

