package com.avinty.repository;

import com.avinty.entity.Employee;
import lombok.extern.log4j.Log4j2;

import javax.naming.NamingException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class EmployeeRepository extends Repository implements EntityRepository<Employee>{

    private static EmployeeRepository instance;

    public static EmployeeRepository getInstance(){
        if(instance == null){
            instance = new EmployeeRepository();
        }
        return instance;
    }

    @Override
    public Employee save(Employee employee) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "INSERT INTO employees (email, password, full_name, dep_id, created_at, created_by, updated_at, updated_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                     Statement.RETURN_GENERATED_KEYS)) {

            stmt.setString(1, employee.getEmail());
            stmt.setString(2, employee.getPassword());
            stmt.setString(3, employee.getFullName());
            stmt.setLong(4, employee.getDepId());
            stmt.setDate(5, Date.valueOf(LocalDate.now()));
            stmt.setLong(6, employee.getCreatedBy());
            stmt.setDate(7, Date.valueOf(LocalDate.now()));
            stmt.setLong(8, employee.getUpdatedBy());

            stmt.executeUpdate();

            ResultSet generatedKeys = stmt.getGeneratedKeys();
            generatedKeys.next();

            return this.findById(generatedKeys.getLong(1));
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Employee findById(Long id) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "SELECT id, email, password, full_name, dep_id, created_at, created_by, updated_at, updated_by FROM employees WHERE id = ?")) {

            stmt.setLong(1, id);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            return this.mapEmployee(rs);
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Employee findByName(String name) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "SELECT id, email, password, full_name, dep_id, created_at, created_by, updated_at, updated_by FROM employees WHERE full_name = ?")) {

            stmt.setString(1, name);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            return this.mapEmployee(rs);
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Employee> listAll() {
        try (Connection con = this.getConnection(); Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery("SELECT id, email, password, full_name, dep_id, created_at, created_by, updated_at, updated_by FROM  employees");
            List<Employee> employees = new ArrayList<>();

            while (rs.next()) {
                employees.add(this.mapEmployee(rs));
            }

            return employees;
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean delete(Long id) {
        try (Connection con = this.getConnection();
             PreparedStatement stmt = con.prepareStatement(
                     "DELETE FROM employees WHERE id = ?")) {
            stmt.setLong(1, id);
            return stmt.executeUpdate() != 0;
        } catch (SQLException e) {
            log.error("SQL Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        } catch (NamingException e) {
            log.error("Naming Exception happened: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private Employee mapEmployee(ResultSet rs) throws SQLException {
        Employee employee = new Employee();
        employee.setId(rs.getLong("id"));
        employee.setEmail(rs.getString("email"));
        employee.setPassword(rs.getString("password"));
        employee.setFullName(rs.getString("full_name"));
        employee.setDepId(rs.getLong("dep_id"));
        employee.setCreatedAt(rs.getDate("created_at"));
        employee.setCreatedBy(rs.getLong("created_by"));
        employee.setUpdatedAt(rs.getDate("updated_at"));
        employee.setUpdatedBy(rs.getLong("updated_by"));

        return employee;
    }
}

