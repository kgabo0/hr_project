package com.avinty.repository;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class Repository {

    private static final String JNDI_DATASOURCE_NAME = "jdbc/client";
    private static final String ROOT_CONTEXT = "java:comp/env";

    private static DataSource dataSource;

    protected Connection getConnection() throws SQLException, NamingException {
        return getDatasource().getConnection();
    }

    private static DataSource getDatasource() throws NamingException {
        if(dataSource == null){
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup(ROOT_CONTEXT);

            dataSource = (DataSource) envCtx.lookup(JNDI_DATASOURCE_NAME);
        }

        return dataSource;
    }

}

