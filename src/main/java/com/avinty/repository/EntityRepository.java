package com.avinty.repository;

import com.avinty.entity.Entity;

import java.util.List;

public interface EntityRepository<T extends Entity>{

    T save(T entity);

    T findById(Long id);

    T findByName(String name);

    List<T> listAll();

    boolean delete(Long id);

}

