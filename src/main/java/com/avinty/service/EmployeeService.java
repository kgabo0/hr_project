package com.avinty.service;

import com.avinty.entity.Employee;
import com.avinty.repository.EmployeeRepository;

import java.util.List;

public class EmployeeService implements EntityService<Employee>{

    private final EmployeeRepository employeeRepository;

    public EmployeeService() {
        this.employeeRepository = new EmployeeRepository();
    }

    @Override
    public Employee save(Employee entity) {
        return this.employeeRepository.save(entity);
    }

    @Override
    public Employee findById(Long id) {
        return this.employeeRepository.findById(id);
    }

    @Override
    public Employee findByName(String name) {
        return this.employeeRepository.findByName(name);
    }

    @Override
    public List<Employee> listAll() {
        return this.employeeRepository.listAll();
    }

    @Override
    public boolean delete(Long id) {
        return this.employeeRepository.delete(id);
    }
}

