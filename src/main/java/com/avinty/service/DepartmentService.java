package com.avinty.service;

import com.avinty.entity.Department;
import com.avinty.repository.DepartmentRepository;

import java.util.List;

public class DepartmentService implements EntityService<Department>{

    private final DepartmentRepository departmentRepository;

    public DepartmentService() {
        this.departmentRepository = new DepartmentRepository();
    }

    @Override
    public Department save(Department entity) {
        return this.departmentRepository.save(entity);
    }

    @Override
    public Department findById(Long id) {
        return this.departmentRepository.findById(id);
    }

    @Override
    public Department findByName(String name) {
        return this.departmentRepository.findByName(name);
    }

    @Override
    public List<Department> listAll() {
        return this.departmentRepository.listAll();
    }

    @Override
    public boolean delete(Long id) {
        return this.departmentRepository.delete(id);
    }
}

